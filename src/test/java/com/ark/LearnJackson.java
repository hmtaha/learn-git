package com.ark;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;

public class LearnJackson {
    @Test
    public void useObjectMapper() throws JsonProcessingException {
        //Given
        ObjectMapper mapper = new ObjectMapper();
        //When
        MyClass muObj = new MyClass(1L, "Hatem", new Date());
        muObj.setMyClass2(new MyClass2(2L, "Hatem", muObj));
        String result = mapper.writer().writeValueAsString(muObj);
        //Then
        System.out.println(result);
    }

    @JsonIdentityInfo(property = "id", generator = ObjectIdGenerators.PropertyGenerator.class)
    private static class MyClass2 {
        private Long id;
        private String name;
        //        @JsonBackReference
        private MyClass myClass;

        public MyClass2() {
        }

        public MyClass2(long id, String name, MyClass myClass) {
            this.id = id;
            this.name = name;
            this.myClass = myClass;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public MyClass getMyClass() {
            return myClass;
        }

        public void setMyClass(MyClass myClass) {
            this.myClass = myClass;
        }
    }

    @JsonIdentityInfo(property = "id", generator = ObjectIdGenerators.PropertyGenerator.class)
    private static class MyClass {
        private Long id;
        private String name;
        @JsonProperty("DateOfBirth")
        private Date dob;
        //        @JsonManagedReference
        private MyClass2 myClass2;

        public MyClass() {
        }

        public MyClass(long id, String name, Date dob) {

            this.id = id;
            this.name = name;
            this.dob = dob;
        }

        public Long getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public Date getDob() {
            return dob;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setDob(Date dob) {
            this.dob = dob;
        }

        public MyClass2 getMyClass2() {
            return myClass2;
        }

        public void setMyClass2(MyClass2 myClass2) {
            this.myClass2 = myClass2;
        }
    }
}
